function validateName(elem){
    var name = elem.value;
    var pattern = /\w/;
    if(!pattern.test(name)){
        alert("Invalid Name");
    }
}

function validateMobNumber(elem){
    var num = elem.value;
    var pattern = /\d{10}/; //only 10 digits
    if(!pattern.test(num)){
        alert("Invalid mobile number");
    }
}

function validateOfficeNumber(elem){
    var num = elem.value;
    var pattern = /\d/; //only digits
    if(!pattern.test(num)){
        alert("Invalid number");
    }
}

function validateEmail(elem){
    var email = elem.value;
    var pattern = /^[\w\.]+@{1}[\w]+\.{1}[a-zA-Z]{2,3}$/; //email id pattern
    if(!pattern.test(email)){
        alert("Invalid Email ID")
    }

}

function validatePassword(elem){
    var pass = elem.value;
    var pattern = /\W/; //Non alphanumeric characters
    var errorMsg = "";
    if(pattern.test(pass)){
        alert("Password should only contain Alphanumeric characters.");
    }
    else if(pass.length<8 || pass.length>12){
        alert("Password should be 8 - 12 characters long.");
    }
}

function validateConPassword(elem){
    var conPass = elem.value;
    var pass ="";
    pass = document.getElementById("pass").value;
    if(conPass.length<1){
        window.alert("This field is compulsory");
    }
    else if(pass != conPass){
        window.alert("Passwords don't match");
    }
}

function calculateAge(){
    var age = 0;
    var year = document.getElementById("dOBYear").value;
    var month = document.getElementById("dOBMonth").value;
    var day = document.getElementById("dOBDate").value;
    var dOB = new Date(String(year)+" "+month+" "+day);
    var currDate = new Date();
    var yearDiff = currDate.getFullYear() - dOB.getFullYear();;
    var monthDiff = currDate.getMonth()-dOB.getMonth();
    var dayDiff = currDate.getDay()-dOB.getDay();
    age = yearDiff + Number(((monthDiff/12)+(dayDiff/365)).toFixed(1));
    document.getElementById("age").value = age;
}

// Form Validation
function runValidation(form){
    var fname = form['fname'].value;
    var namePatt = /\w/;
    var lname = form['lname'].value;
    var phone1 = form['phone1'].value;
    var phone1Patt = /\d{10}/;
    var phone2 = form['phone2'].value;
    var phone2Patt = /\d/;
    var email = form['email'].value;
    var emailPatt = /^[\w\.]+@{1}[\w]+\.{1}[a-zA-Z]{2,3}$/;
    var password = form['password'].value;
    var conpassword = form['conpassword'].value;
    var dobmonth = form['dobmonth'].value;
    var dobdate = form['dobdate'].value;
    var gender = form['gender'];
    var interests = form['interests[]'];
    var aboutyou = form['aboutyou'].value;

    if(fname == ""){
        alert("First Name is required");
        return false;
    }

    else if(!(namePatt.test(fname) && namePatt.test(lname))){
        alert("Invalid name");
        return false;
    }

    else if(lname == ""){
        alert("Last Name is required");
        return false;
    }

    else if(phone1 == ""){
        alert("Phone Number is required");
        return false;
    }

    else if(!phone1Patt.test(phone1)){
        alert("Phone number invalid");
        return false;
    }

    else if(phone2 == ""){
        alert("Office Number is required");
        return false;
    }

    else if(!phone2Patt.test(phone2)){
        alert("Office number invalid");
        return false;
    }

    else if(email == ""){
        alert("Email is required");
        return false;
    }
    
    else if(!emailPatt.test(email)){
        alert("Email is invalid");
        return false;
    }
    else if(password == ""){
        alert("Password is required");
        return false;
    }

    else if(conpassword == ""){
        alert("Re-type your Password to confirm");
        return false;
    }

    else if(password != conpassword){
        alert("Passwords don't match!");
        return false;
    }

    else if(dobmonth == "select" || dobdate == "select"){
        alert("Birthdate is required");
        return false;
    }
    
    else if(gender[0].checked == false && gender[1].checked == false){
        alert("Select a gender");
        return false;
    }

    else if(aboutyou == ""){
        alert("\"About You\" section should not be left empty");
        return false;
    }

    if(interests[0].checked == false && interests[1].checked == false && interests[2].checked == false){
        alert("Select atleast 1 interest");
        return false;
    }

    else {
        console.log("Form submitted");
        return true;}
}

